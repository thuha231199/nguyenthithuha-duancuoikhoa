import { useMediaQuery } from 'react-responsive'

export const LargeDesktop = ({ children }) => {
  const isDesktop = useMediaQuery({ minWidth: 1201 })
  return isDesktop ? children : null
}
export const Desktop = ({ children }) => {
  const isDesktop = useMediaQuery({ minWidth: 991, maxWidth: 1200 })
  return isDesktop ? children : null
}
export const Tablet = ({ children }) => {
  const isTablet = useMediaQuery({ minWidth: 766, maxWidth: 990 })
  return isTablet ? children : null
}
export const Mobile = ({ children }) => {
  const isMobile = useMediaQuery({ minWidth: 576, maxWidth: 765 })
  return isMobile ? children : null
}
export const SmallMobile = ({ children }) => {
  const isMobile = useMediaQuery({ maxWidth: 575 })
  return isMobile ? children : null
}

export const MobileSearch = ({ children }) => {
  const isMobileSearch = useMediaQuery({ maxWidth: 768 })
  return isMobileSearch ? children : null
}

export const Default = ({ children }) => {
  const isNotMobile = useMediaQuery({ minWidth: 768 })
  return isNotMobile ? children : null
}

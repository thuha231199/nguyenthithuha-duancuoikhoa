import { Tag } from "antd"

export const headerColumns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        responsive: ["md"]
    },
    {
        title: 'Phone number',
        dataIndex: 'phone',
        key: 'phone',
        responsive: ["sm"]
    },
    {
        title: 'Role',
        dataIndex: 'role',
        key: 'role',
        render: (loai) => {
            if (loai === 'ADMIN') {
                return <Tag className="font-medium" color="red">{loai}</Tag>
            }
            else {
                return <Tag className="font-medium" color="green">{loai}</Tag>
            }
        }
    },
    {
        title: 'Action',
        dataIndex: 'action',
        key: 'action',
    },
]

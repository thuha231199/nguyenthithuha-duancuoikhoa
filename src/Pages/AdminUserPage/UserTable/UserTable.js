import React, { useEffect, useState } from 'react'
import { adminServ } from '../../../Services/adminService';
import { useDispatch } from 'react-redux';
import { setLoadingOff, setLoadingOn } from '../../../Toolkits/spinnerSlice';
import { Button, Table, Tooltip, message, Modal, Input } from 'antd';
import { headerColumns } from '../utils';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import '../../../css/userTable.css'
import { showEditModal } from '../../../Toolkits/adminSlice';
import { useSearchParams } from 'react-router-dom';

export default function UserTable() {
    const [userList, setUserList] = useState([])
    let dispatch = useDispatch();
    const [searchValues, setSearchValues] = useSearchParams();
    useEffect(() => {
        fetchUserList()
    }, [])
    let fetchUserList = () => {
        dispatch(setLoadingOn());
        adminServ.getUserList()
            .then((res) => {
                dispatch(setLoadingOff());
                let list = res.data.content.map((user) => {
                    return {
                        ...user, action: (
                            <div className='space-x-2 flex items-center '>
                                <Tooltip title="Edit">
                                    <Button onClick={() => { handleEditUser(user) }} shape="circle" icon={<EditOutlined />} className='edit flex items-center justify-center' />
                                </Tooltip>
                                <Tooltip title="Delete">
                                    <Button onClick={() => { showConfirm(user.id) }} shape="circle" icon={<DeleteOutlined />} className='delete flex items-center justify-center' />
                                </Tooltip>
                            </div>
                        )
                    }
                })
                setUserList(list);
            })
            .catch((err) => {
                dispatch(setLoadingOff());
                console.log(err);
            });
    }
    let handleDeleteUser = (useId) => {
        dispatch(setLoadingOn())
        adminServ.deleteUser(useId)
            .then((res) => {
                dispatch(setLoadingOff());
                message.success("Xóa người dùng thành công");
                fetchUserList();
            })
            .catch((err) => {
                dispatch(setLoadingOff());
                message.error("Đã có lỗi xảy ra");
                console.log(err);
            });
    }
    let handleEditUser = (user) => {
        dispatch(showEditModal(user));
    }
    let handleSearchOnChange = (e) => {
        let { value } = e.target;
        setSearchValues({ name: value });
        if (!value) {
            fetchUserList();
        }
        else {
            adminServ.getSearchUser(value)
                .then((res) => {
                    let list = res.data.content.map((user) => {
                        return {
                            ...user, action: (
                                <div className='space-x-2 flex items-center '>
                                    <Tooltip title="Edit">
                                        <Button onClick={() => { handleEditUser(user) }} shape="circle" icon={<EditOutlined />} className='edit flex items-center justify-center' />
                                    </Tooltip>
                                    <Tooltip title="Delete">
                                        <Button onClick={() => { showConfirm(user.id) }} shape="circle" icon={<DeleteOutlined />} className='delete flex items-center justify-center' />
                                    </Tooltip>
                                </div>
                            )
                        }
                    })
                    setUserList(list);
                })
                .catch((err) => {
                    console.log(err);
                });
        }
    }

    const { confirm } = Modal;
    const showConfirm = (userId) => {
        confirm({
            icon: '',
            content: (
                <div className='space-y-3'>
                    <h1>Xác nhận xóa người dùng này</h1>
                </div>
            ),
            onOk() {
                handleDeleteUser(userId);
            },
            onCancel() { }
        });
    };

    return (
        <div id='adminTable' className='p-3 bg-white mt-3'>
            <Input onChange={handleSearchOnChange} className='py-2 mb-2' placeholder='Tìm kiếm người dùng' />
            <Table dataSource={userList} columns={headerColumns} />
        </div>
    );
};

import React, { useState } from 'react'
import { Button, Modal, Form, Input, message, Radio } from 'antd';
import '../../../css/addEditModal.css'
import { useDispatch, useSelector } from 'react-redux';
import { handleCloseModal, showAddModal } from '../../../Toolkits/adminSlice';
import { adminServ } from '../../../Services/adminService';

export default function AddEditTypeJobModal() {
    let dispatch = useDispatch();
    let { isOpenM, typesOfJob } = useSelector(state => state.adminSlice)
    console.log("🚀 ~ file: AddEditTypeJobModal.js:11 ~ AddEditTypeJobModal ~ typesOfJob:", typesOfJob)

    const handleAddJob = () => {
        dispatch(showAddModal());
    };
    const handleCancel = () => {
        dispatch(handleCloseModal());
        window.location.reload();
    };

    const onFinish = (values) => {
        if (typesOfJob) {
            adminServ.updateTypesOfJob(values.id, values)
                .then((res) => {
                    message.success('Cập nhật loại công việc thành công');
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                    window.location.reload();
                })
                .catch((err) => {
                    message.error("Đã có lỗi xảy ra");
                });
        }
        else {
            adminServ.addTypesOfJob(values)
                .then((res) => {
                    message.success('Thêm loại công việc thành công');
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                })
                .catch((err) => {
                    message.error("Đã có lỗi xảy ra");
                });
        }

    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div id='addEditJobModal' className='addEditModal'>
            <Button onClick={handleAddJob} className='add'>
                Thêm loại công việc
            </Button>
            <Modal title={typesOfJob ? "Chỉnh sửa thông tin loại công việc" : "Thêm loại công việc mới"} open={isOpenM} footer={false} onCancel={handleCancel}>
                <Form
                    name="basic"
                    labelCol={{
                        span: 10,
                    }}
                    wrapperCol={{
                        span: 24,
                    }}
                    style={{
                        maxWidth: 600,
                    }}
                    initialValues={{
                        id: typesOfJob ? typesOfJob.id : null,
                        tenLoaiCongViec: typesOfJob ? typesOfJob.tenLoaiCongViec : null,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        label="Mã loại công việc"
                        name="id"
                        rules={[
                            {
                                required: false,
                                message: 'Mục không được để trống!',
                            },
                        ]}

                    >
                        <Input disabled={typesOfJob ? true : false} />
                    </Form.Item>

                    <Form.Item
                        label="Tên loại công việc"
                        name="tenLoaiCongViec"
                        rules={[
                            {
                                required: true,
                                message: 'Mục không được để trống!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        wrapperCol={{
                            offset: 8,
                            span: 16,
                        }}
                    >
                        <Button htmlType="submit" className='addEditBtn mr-3'>
                            {typesOfJob ? "Cập nhật" : "Thêm"}
                        </Button>
                        <Button onClick={handleCancel}>Hủy</Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    )
}

import React from 'react'
import { DownOutlined } from '@ant-design/icons';
import { Button, Dropdown, Space, Switch } from 'antd';
import '../../../css/filterBar.css'
import { dropdownMenu, switchBtn } from '../../../assets/filter_button';

const items = [
    {
        label: <div className='filterOption'>All Categories</div>,
        key: '1',
    },
    {
        label: (
            <div className='filterOption flex justify-between'>Web Programing<span className='number'>(20,566)</span></div>
        ),
        key: '2',
    },
    {
        label: (
            <div className='filterOption flex justify-between'>Data Entry<span className='number'>(12,566)</span></div>
        ),
        key: '3',
    },
];
const menuProps = {
    items,
    selectable: true,
    defaultSelectedKeys: ['1'],
};

export default function FilterBar({ keyWord, numOfJob }) {
    let renderDropdownMenu = () => {
        return dropdownMenu.map((item, index) => {
            return (
                <Dropdown menu={menuProps} key={index}>
                    <Button>
                        <Space>
                            {item}
                            <DownOutlined />
                        </Space>
                    </Button>
                </Dropdown>
            )
        })
    }
    let renderSwitchBtn = () => {
        return switchBtn.map((item, index) => {
            return (
                <div key={index} className='space-x-2'>
                    <Switch />
                    <span>{item}</span>
                </div>
            )
        })
    }
    return (
        <div id='filterBar' className='container mb-3'>
            <div className=' mx-auto xxl:w-11/12'>
                <h2 className='title px-3'>Result for "{keyWord}"</h2>
                <div className="filterCategory 2xl:flex justify-between items-center px-3 mt-4">
                    <div className='dropbox flex justify-start items-center space-x-4 mb-3'>
                        {renderDropdownMenu()}
                    </div>
                    <div className="switchBtn flex 2xl:justify-end justify-start items-center space-x-7 mb-3">
                        {renderSwitchBtn()}
                    </div>
                </div>
                <div className='pre-title flex justify-between items-center px-3'>
                    <p>{numOfJob} services available</p>
                    <div className='sortBar flex justify-start items-center'>
                        <p>Sort by</p>
                        <select name="" id="">
                            <option value="relevance">Relevance</option>
                            <option value="bestSelling">Best Selling</option>
                            <option value="newArrivals">New Arrivals</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    )
}

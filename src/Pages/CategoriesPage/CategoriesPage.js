import React , {useEffect , useState} from 'react'
import { workServ } from '../../Services/WorkService'
import { NavLink, useParams } from 'react-router-dom'
import { useDispatch } from 'react-redux';
import { setHomePage } from '../../Toolkits/HomeSlice';
import Header from '../../Components/Header/Header';
import CategoriesMenu from '../../Components/CategoriesMenu/CategoriesMenu';
import Footer from '../../Components/Footer/Footer';
import { dataFilter, dataSwitch } from '../../assets/data_filter';
import { Dropdown ,  Space , Switch} from 'antd';
import '../../css/titlePage.css' ; 
function CategoriesPage() {
    const items = [
        {
            id : 1 , 
            label : 'Web programing (20,566)'
        } , 
        {
            id : 2 , 
            label : 'Data Entry (12,566)'
        }
    ]
    const item_sort = [
        {
            id : 1 , 
            label : 'Best Selling'
        } , 
        {
            id : 2 , 
            label : 'New Arrival'
        }
    ]
    let dispatch = useDispatch() ; 
    dispatch(setHomePage()) ; 
    let params = useParams() ; 
    let id = params.id ; 
    const [listJob, setListJob] = useState([]) ; 
    const [titleJob, setTitleJob] = useState("") ; 
    let fetchJobByType = async() => {
        try {
            let res = await workServ.getJobByType(id) ; 
            console.log(res.data.content);
            setListJob(res.data.content) ;
            setTitleJob(res.data.content[0].tenChiTietLoai) ;
        } catch (error) {
            console.log(error);
        }
    }
    useEffect(() => {
    fetchJobByType() ; 
    }, [id])
    const onChange = (checked) => {
        console.log(`switch to ${checked}`);
    };
    return (
        <>
            <Header/>
            <CategoriesMenu/>
            <div className='categories pt-32'>
                <div className="container">
                    <h1 className='font-bold text-2xl mb-5'>{titleJob}</h1>
                    <div className="categories-filter-switch flex justify-between items-center flex-wrap">
                        <div className="categories-filter mb-2">
                            {dataFilter.map((item) => {
                                return (
                                    <Dropdown
                                        menu={{items}}
                                        trigger={['click']}
                                        key = {item.id}
                                        className='border p-2 mr-2 cursor-pointer'
                                    >
                                        <a onClick={(e) => e.preventDefault()}>
                                        <Space className='font-medium text-lg text-gray-600'>
                                            {item.content}
                                            <i className="fa fa-angle-down"></i>
                                        </Space>
                                        </a>
                                    </Dropdown>
                                )
                            })}
                        </div>
                        <div className="categories-switch flex items-center">
                            {dataSwitch.map((item) => {
                                return (
                                    <div className='switch_item mr-4' key={item.id}>
                                        <Switch className='mr-2' key={item.id} onChange={onChange} />
                                        <span>{item.content}</span>
                                    </div>
                                    
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
            <div className="categories-sort mt-10">
                <div className="container flex justify-between items-center">
                    <span className='text-gray-600'>{listJob.length} service available</span>
                    <div className='sort'>
                        <span className='text-gray-600'>Sort by </span>
                        <Dropdown
                            menu={{items : item_sort}}
                            trigger={['click']}
                            className='cursor-pointer'
                        >
                            <a onClick={(e) => e.preventDefault()}>
                            <Space className='font-bold text-lg text-black'>
                                Relevance
                                <i className="fa fa-angle-down"></i>
                            </Space>
                            </a>
                        </Dropdown>
                    </div>
                </div>
            </div>
            <div className="job-categories py-10">
                <div className="container">
                    <div className='content_categories grid sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-4 gap-x-5 max-sm:gap-y-5'>
                        {listJob.map((job , index) => {
                                return (
                                    <div className='content_item border' key={index}>
                                        <img className='w-full' src={job.congViec.hinhAnh} alt="job_img" />
                                        <div className='item_info p-3 border-b space-y-5 h-48'>
                                            <div className='flex items-center'>
                                                <img style={{borderRadius : '50%'}} className='w-8 mr-2' src={job.avatar} alt="avatar_img" />
                                                <div>
                                                    <span className='text-sm font-bold'>{job.tenNguoiTao}</span>
                                                    <p className='text-sm text-gray-600'>level {job.congViec.saoCongViec} Seller</p>
                                                </div>
                                            </div>
                                            <div className="link">
                                                <NavLink className='text-gray-600' to={`/jobDetail/${job.id}`}>
                                                    {job.congViec.tenCongViec}
                                                </NavLink>
                                            </div>
                                            <div>
                                                <span className='text-yellow-400'>{job.congViec.saoCongViec}<i className="fa fa-star" aria-hidden="true"></i></span>
                                                <span className='text-gray-400'>({job.congViec.danhGia})</span>
                                            </div>
                                        </div>
                                        <div className="cost flex justify-between items-center p-2">
                                            <i className="fa fa-heart text-gray-600" aria-hidden="true"></i>
                                            <div className='cost-info'>
                                                STARTING AT <span className='font-bold text-lg'>US${job.congViec.giaTien}</span>
                                            </div>
                                        </div>
                                    </div>
                                )
                        })}
                    </div>
                </div>
            </div>
            <Footer/>
        </>
    )
}

export default CategoriesPage
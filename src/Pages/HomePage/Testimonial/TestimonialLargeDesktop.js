import React, { useState } from 'react'
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { dataModal } from '../../../assets/data_modal';
import { useDispatch } from 'react-redux';
import { handleOpenModal } from '../../../Toolkits/modalSlice';
import ModalPopUp from '../../../Components/Modal/Modal';

export default function TestimonialLargeDesktop() {
    const disptach = useDispatch();
    let handleShowModal = (modalId) => {
        disptach(handleOpenModal(modalId));
    }
    let renderThumbnail = () => {
        return dataModal.map((item, index) => {
            return (
                <div key={index} >
                    <div className='item w-10/12 mx-auto grid grid-cols-12 gap-3 items-center'>
                        <div className='thumbnail col-span-6'>
                            <button onClick={() => { handleShowModal(item.media) }} className='item__button w-full'>
                                <img src={item.thumbnail} alt="" />
                            </button>
                        </div>
                        <div className='content col-span-6 space-y-3'>
                            <div className='title flex items-center space-x-3'>
                                <h5>{item.title}</h5>
                                <span><img src={item.logo} alt="" className='h-full' /></span>
                            </div>
                            <p>"{item.quote}"</p>
                        </div>
                    </div>
                </div>
            )
        })
    }

    return (
        <div id='testimonial' className='testimonialLargeDesktop'>
            <div className="container">
                <div className='mx-auto pt-4 mb-10'>
                    <OwlCarousel className='owl-theme' loop nav dots={false} items={1}>
                        {renderThumbnail()}
                    </OwlCarousel>
                </div>
            </div>
            <ModalPopUp />
        </div>
    )
}

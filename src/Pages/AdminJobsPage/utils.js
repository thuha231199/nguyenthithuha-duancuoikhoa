import { StarFilled } from "@ant-design/icons";
import { Tag } from "antd"

export const headerColumns = [
    {
        title: 'Mã loại công việc',
        dataIndex: 'maChiTietLoaiCongViec',
        key: 'maChiTietLoaiCongViec',
        render: (id) => {
            switch (id) {
                case 1:
                    return <Tag className="font-medium" color="green">{id}</Tag>
                    break;
                case 2:
                    return <Tag className="font-medium" color="volcano">{id}</Tag>
                    break;
                case 3:
                    return <Tag className="font-medium" color="blue">{id}</Tag>
                    break;
                case 4:
                    return <Tag className="font-medium" color="magenta">{id}</Tag>
                    break;
                case 5:
                    return <Tag className="font-medium" color="purple">{id}</Tag>
                    break;

                default:
                    return <Tag className="font-medium" color="cyan">{id}</Tag>
                    break;
            }
        }
    },
    {
        title: 'Tên công việc',
        dataIndex: 'tenCongViec',
        key: 'tenCongViec',
        width: '250px',

    },
    {
        title: 'Hình ảnh',
        dataIndex: 'hinhAnh',
        key: 'hinhAnh',
        render: img => <img src={img} alt="" />,
        responsive: ["md"],
        width: '150px',
    },
    {
        title: 'Giá tiền',
        dataIndex: 'giaTien',
        key: 'giaTien',
        render: gia => <Tag className="font-medium" color="green">${gia}</Tag>,
    },
    {
        title: 'Sao công việc',
        dataIndex: 'saoCongViec',
        key: 'saoCongViec',
        render: sao => <Tag className="font-medium flex items-center w-fit" color="gold"><StarFilled />{sao}</Tag>,
        responsive: ["sm"]
    },
    {
        title: 'Mô tả ngắn',
        dataIndex: 'moTaNgan',
        key: 'moTaNgan',
        responsive: ["lg"],
        width: '400px',
        className: 'moTa',
    },
    {
        title: 'Action',
        dataIndex: 'action',
        key: 'action',
    },
]

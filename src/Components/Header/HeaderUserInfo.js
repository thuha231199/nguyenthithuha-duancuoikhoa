import React from 'react'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import { localUserServ } from '../../Services/localService';
import { Dropdown, Space } from 'antd';
import { DownOutlined } from '@ant-design/icons';

const items = [
    {
        key: '1',
        label: (<NavLink to='/profile'>Cập nhật thông tin</NavLink>),
    },
    {
        key: '2',
        label: (<NavLink to='/admin-users'>Quản trị</NavLink>),
    },
];

function HeaderUserInfo() {
    let userInfo = localUserServ.get();
    if (userInfo) {
        return (
            <>
                <Dropdown
                    menu={{
                        items,
                    }}
                >
                    <Button style={{ border: 'none', outline: 'none' }} className='hover:bg-transparent nav__link'>
                        <Space className='font-bold'>
                            {userInfo ? userInfo.user.name : null}
                            <DownOutlined />
                        </Space>
                    </Button>
                </Dropdown>
            </>
        )
    } else {
        return (
            <>
                <NavLink to={'/login'} className='p-2'>
                    <span className='nav__link'>Sign in</span>
                </NavLink>
                <NavLink to={'/register'} className='cursor-pointer'>
                    <Button className='text-green-500 hover:text-white ml-4' variant='outline-success'>Join</Button>
                </NavLink>
            </>
        )
    }
}

export default HeaderUserInfo
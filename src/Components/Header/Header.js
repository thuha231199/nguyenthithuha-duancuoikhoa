import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Offcanvas from 'react-bootstrap/Offcanvas';
import Search from './Search';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import HeaderUserInfo from './HeaderUserInfo';

function Header() {
    let navigate = useNavigate();
    let { isHomePage1 } = useSelector(state => state.HomeSlice);
    let renderHeaderActive = () => {
        if (isHomePage1) {
            return 'header header__active'
        } else {
            return 'header'
        }
    };
    let handleBackToHome = (e) => {
        e.preventDefault();
        navigate('/')
    }
    return (
        <>
            {['lg'].map((expand) => (
                <Navbar id='navbar__header' key={expand} expand={expand} className={renderHeaderActive()}>
                    <Container>
                        <Navbar.Brand onClick={handleBackToHome} className='nav__brand cursor-pointer'>Fiverr</Navbar.Brand>
                        <Search />
                        <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
                        <Navbar.Offcanvas
                            id={`offcanvasNavbar-expand-${expand}`}
                            aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
                            placement="start"
                        >
                            <Offcanvas.Header closeButton>
                                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
                                    Offcanvas
                                </Offcanvas.Title>
                            </Offcanvas.Header>
                            <Offcanvas.Body>
                                <Nav className="justify-content-end flex-grow-1 pe-2">
                                    <Nav.Link className='nav__link' href="#action1">Fiverr Business</Nav.Link>
                                    <Nav.Link className='nav__link' href="#action2">Explore</Nav.Link>
                                    <Nav.Link className='nav__link' href="#action2">English</Nav.Link>
                                    <Nav.Link className='nav__link' href="#action2">US$ USD</Nav.Link>
                                    <Nav.Link className='nav__link' href="#action2">Become a Seller</Nav.Link>
                                    <HeaderUserInfo />
                                </Nav>
                            </Offcanvas.Body>
                        </Navbar.Offcanvas>
                    </Container>
                </Navbar>
            ))}
        </>
    );
}

export default Header;
import React from 'react'
import { useSelector } from 'react-redux'
import { GridLoader } from 'react-spinners'

function Spinner() {
    let { isLoading } = useSelector((state) => state.spinnerSlice);
    if (isLoading) {
        return (
            <div style={{ background: 'rgb(255, 255, 255 , 0.7)' }} className='w-screen h-screen fixed top-0 left-0 flex justify-center items-center z-50'>
                <GridLoader color="#1dbf73" />
            </div>
        )
    } else {
        return <></>
    }
}

export default Spinner
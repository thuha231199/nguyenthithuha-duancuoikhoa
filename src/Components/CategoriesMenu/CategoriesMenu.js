import React, { useState , useEffect } from 'react'
import { workServ } from '../../Services/WorkService';
import ItemMenuDropdown from './ItemMenuDropdown';
import { useSelector } from 'react-redux';

function CategoriesMenu() {
    let {isHomePage1} = useSelector(state => state.HomeSlice) ; 
    const [typeWork, setTypeWork] = useState([]) ; 
    let renderCategoryActive = () => {
        if (isHomePage1) {
            return 'categoriesMenu categoriesMenu-active'
        }else {
            return 'categoriesMenu'
        }
    }
    useEffect(() => {
        let fetchMenuTypeWork = async () => {
            try {
                let response = await workServ.getMenuTypeWork() ; 
                console.log(response.data.content);
                setTypeWork(response.data.content) ; 
            } catch (error) {
                console.log(error);
            }
        }
        fetchMenuTypeWork() ; 
    }, [])
    let renderTypeWork = () => {
        return typeWork.slice(0,5).map((work , index)=> {
            return <ItemMenuDropdown key={index} work = {work}/>
        })
    }
    return (
        <div id='category' className={renderCategoryActive()}>
            {renderTypeWork()} 
        </div>
    )
}

export default CategoriesMenu
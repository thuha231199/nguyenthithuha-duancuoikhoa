import React from 'react'
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';

function Checkout({detailedJob}) {
    return (
        <>
            <Tabs
            defaultActiveKey="basic"
            transition={false}
            id="noanim-tab-example"
            className="mb-3"
        >
            <Tab eventKey="basic" title="Basic">
                <div className="content p-3">
                    <div className="checkout-body flex items-center justify-between my-2">
                        <h1 className='text-xl font-bold'>Basic</h1>
                        <span className='text-xl font-bold'>US${detailedJob.congViec.giaTien}</span>
                    </div>
                    <p className='short-desc text-gray-500 text-base my-2'>{detailedJob.congViec.moTaNgan}</p>
                    <div className="additional-info flex items-center justify-between">
                        <div className="info-days">
                            <i className="fas fa-clock mr-2 "></i>
                            <span>14 Days Delivery</span>
                        </div>
                        <div className="info-revisions">
                            <i className="fa fa-redo mr-2"></i>
                            <span>Unlimited Revisions</span>
                        </div>
                    </div>
                    <ul className="list__feature my-4">
                        <li>
                            <i className="fa fa-check text-green-500 text-base font-bold  mr-2" aria-hidden="true"></i>
                            <span className='text-gray-500'>Good feature</span>
                        </li>
                        <li>
                            <i className="fa fa-check text-green-500 text-base font-bold mr-2" aria-hidden="true"></i>
                            <span className='text-gray-500'>Good feature</span>
                        </li>
                        <li>
                            <i className="fa fa-check text-green-500 text-base font-bold  mr-2" aria-hidden="true"></i>
                            <span className='text-gray-500'>Good feature</span>
                        </li>
                    </ul>
                    <button className='w-full py-2 bg-green-500 text-white rounded hover:bg-green-600 mb-4'>Continue (US${detailedJob.congViec.giaTien})</button>
                    <button className='border-none w-full bg-transparent text-green-500'>Compare Packages</button>
                </div>
            </Tab>
            <Tab eventKey="standard" title="Standard">
                <div className="content p-3">
                    <div className="checkout-body flex items-center justify-between my-2">
                        <h1 className='text-xl font-bold'>Basic</h1>
                        <span className='text-xl font-bold'>US${detailedJob.congViec.giaTien}</span>
                    </div>
                    <p className='short-desc text-gray-500 text-base my-2'>{detailedJob.congViec.moTaNgan}</p>
                    <div className="additional-info flex items-center justify-between">
                        <div className="info-days">
                            <i className="fas fa-clock mr-2 "></i>
                            <span>14 Days Delivery</span>
                        </div>
                        <div className="info-revisions">
                            <i className="fa fa-redo mr-2"></i>
                            <span>Unlimited Revisions</span>
                        </div>
                    </div>
                    <ul className="list__feature my-4">
                        <li>
                            <i className="fa fa-check text-green-500 text-base font-bold  mr-2" aria-hidden="true"></i>
                            <span className='text-gray-500'>Good feature</span>
                        </li>
                        <li>
                            <i className="fa fa-check text-green-500 text-base font-bold mr-2" aria-hidden="true"></i>
                            <span className='text-gray-500'>Good feature</span>
                        </li>
                        <li>
                            <i className="fa fa-check text-green-500 text-base font-bold  mr-2" aria-hidden="true"></i>
                            <span className='text-gray-500'>Good feature</span>
                        </li>
                    </ul>
                    <button className='w-full py-2 bg-green-500 text-white rounded hover:bg-green-600 mb-4'>Continue (US${detailedJob.congViec.giaTien})</button>
                    <button className='border-none w-full bg-transparent text-green-500'>Compare Packages</button>
                </div>
            </Tab>
            <Tab eventKey="premium" title="Premium">
                <div className="content p-3">
                    <div className="checkout-body flex items-center justify-between my-2">
                        <h1 className='text-xl font-bold'>Basic</h1>
                        <span className='text-xl font-bold'>US${detailedJob.congViec.giaTien}</span>
                    </div>
                    <p className='short-desc text-gray-500 text-base my-2'>{detailedJob.congViec.moTaNgan}</p>
                    <div className="additional-info flex items-center justify-between">
                        <div className="info-days">
                            <i className="fas fa-clock mr-2 "></i>
                            <span>14 Days Delivery</span>
                        </div>
                        <div className="info-revisions">
                            <i className="fa fa-redo mr-2"></i>
                            <span>Unlimited Revisions</span>
                        </div>
                    </div>
                    <ul className="list__feature my-4">
                        <li>
                            <i className="fa fa-check text-green-500 text-base font-bold  mr-2" aria-hidden="true"></i>
                            <span className='text-gray-500'>Good feature</span>
                        </li>
                        <li>
                            <i className="fa fa-check text-green-500 text-base font-bold mr-2" aria-hidden="true"></i>
                            <span className='text-gray-500'>Good feature</span>
                        </li>
                        <li>
                            <i className="fa fa-check text-green-500 text-base font-bold  mr-2" aria-hidden="true"></i>
                            <span className='text-gray-500'>Good feature</span>
                        </li>
                    </ul>
                    <button className='w-full py-2 bg-green-500 text-white rounded hover:bg-green-600 mb-4'>Continue (US${detailedJob.congViec.giaTien})</button>
                    <button className='border-none w-full bg-transparent text-green-500'>Compare Packages</button>
                </div>
            </Tab>
        </Tabs>        
        </>
    )
}

export default Checkout
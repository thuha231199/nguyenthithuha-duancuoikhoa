import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    userInfo: null,
    jobInfo: null,
    typesOfJob: null,
    serviceInfo: null,
    isOpenM: false,
}

const adminSlice = createSlice({
    name: "adminSlice",
    initialState,
    reducers: {
        showEditModal: (state, action) => {
            state.isOpenM = true;
            state.userInfo = action.payload;
            state.jobInfo = action.payload;
            state.typesOfJob = action.payload;
            state.serviceInfo = action.payload;
        },
        showAddModal: (state, action) => {
            state.isOpenM = true;
            state.userInfo = null;
            state.jobInfo = null;
            state.typesOfJob = null;
            state.serviceInfo = null;
        },
        handleCloseModal: (state, action) => {
            state.isOpenM = false;
            state.userInfo = null;
            state.jobInfo = null;
            state.typesOfJob = null;
            state.serviceInfo = null;
        }
    }
});

export const { showEditModal, showAddModal, handleCloseModal } = adminSlice.actions

export default adminSlice.reducer

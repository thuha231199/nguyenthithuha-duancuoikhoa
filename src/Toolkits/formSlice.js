import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    formVal: '',
}

const formSlice = createSlice({
    name: "formSlice",
    initialState,
    reducers: {
        getFormValue: (state, action) => {
            state.formVal = action.payload;
        }
    }
});

export const { getFormValue } = formSlice.actions

export default formSlice.reducer
